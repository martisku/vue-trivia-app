import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import GameIntro from './views/GameIntro'
import GameResults from './views/GameResults'
import GameStarted from './views/GameStarted'
import vuetify from './plugins/vuetify';
import Vuetify from 'vuetify/lib';

Vue.use(VueRouter);
Vue.config.productionTip = false

const routes = [{
  path: '/',
  component: GameIntro
},
{
  path: '/quiz',
  component: GameStarted
},
{
  path: '/results',
  name: 'GameResults',
  component: GameResults,
  props: true
}
]

const router = new VueRouter({
  routes
});
new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')

Vue.use(Vuetify)